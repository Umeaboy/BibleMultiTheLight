3.21 - 20190922


== English ==

• You do not have to restart the application when you change some preferences
• The language selected in the application may be different from the language of your system
• By selecting a Bible, this will define the language used in the application


== Français ==

• Vous ne devez pas redémarrer l'application lorsque vous modifiez certaines préférences
• La langue sélectionnée dans l'application peut être différente de la langue de votre système
• En sélectionnant une Bible, cela définira la langue utilisée dans l'application


== Italiano ==

• Non è necessario riavviare l'applicazione quando si modificano alcune preferenze
• La lingua selezionata nell'applicazione potrebbe essere diversa dalla lingua del sistema
• Selezionando una Bibbia, questo definirà la lingua utilizzata nell'applicazione


== Español ==

• No tiene que reiniciar la aplicación cuando cambia algunas preferencias
• El idioma seleccionado en la aplicación puede ser diferente del idioma de su sistema
• Al seleccionar una Biblia, esto definirá el idioma utilizado en la aplicación


== Portugués ==

• Você não precisa reiniciar o aplicativo ao alterar algumas preferências
• O idioma selecionado no aplicativo pode ser diferente do idioma do seu sistema
• Ao selecionar uma Bíblia, isso definirá o idioma usado no aplicativo
