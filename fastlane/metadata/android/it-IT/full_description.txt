Bibbia multi lingue, gratuita, offline, senza pubblicità, interamente in inglese, francese, italiano, spagnolo, portoghese, hindi.

King James Version, Segond, Diodati, Valera, Almeida, Ostervald, Schlachter, Bibbia in arabo, Bibbia hindi, Bibbia cinese, Bibbia tedesca, Bibbia russa.

Facile da usare con funzioni di ricerca veloce e di condivisione, piani de lettura, Bibbia audio, articoli, riferimenti incrociati.

Funziona anche su Android TV, Chromebook.

