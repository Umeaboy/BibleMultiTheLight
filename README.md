## Description
Bible multi languages, free, offline, no advertising, completely in English, French, Italian, Spanish, Portuguese, Hindi.  
King James Version, Segond, Diodati, Valera, Almeida, Schlachter, Arabic Bible, Hindi Bible, Chinese Bible, Japanese Bible, Russian Bible.       
Easy to use with quick search and share, plans of reading, articles.


## Installation
Google play store, F-Droid, Gitlab
* To install on Android TV if not available on the store:  
  https://gitlab.com/hotlittlewhitedog/BibleMultiTheLight/-/blob/master/apk/readme-android-tv.md


## License
<img src="https://gnu.org/graphics/gplv3-127x51.png" />
[GPLv3](http://www.gnu.org/licenses/gpl-3.0.html) 


## Screenshots
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa1.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa2.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa3.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa4.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa5.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa6.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa7.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb01.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb02.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb03.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb04.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb05.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb06.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb07.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb08.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb09.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb10.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb11.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb12.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb13.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb14.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb15.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb16.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb17.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb18.png)
